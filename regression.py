import numpy as np
from numpy.linalg import inv

# this function can handle univariate nonlinear regression and multivariate linear regression
#forrmula:  βj := βj - α[(1/m)Σ(yi-f(xi))(xi)+(λ/m)βj]
def gradient_descent(x, y, alpha, landa, degree, iters):
    #x: train inputs | matrix with m*(degree+1) dimention
    #y: train outputs | array
    #alpha: learning rate
    #landa: regularization coefficient
    #degree: polynomial degree
    #iters: number of iterations
    theta = np.ones(degree + 1)
    weighs = np.ones((1, degree + 1))
    m = len(y)  # data length
    for i in range(0, iters - 1):
        y_hat = np.dot(x, theta)
        loss = y_hat - y
        # print("loss:", loss)
        MSE = np.sum(loss ** 2) / (2 * m)
        # print("iteration:", i, "MSE:", MSE)
        gradient = np.dot(x.transpose(), loss) / m
        theta = theta - alpha * (gradient + (landa/m)*theta)
        weighs = np.append(weighs, [theta], axis=0)

    final_y_hat = np.dot(x, theta)

    return (theta, final_y_hat, weighs, MSE)

# Normal Equation:
# theta = inv(x^T * x) * x^T * y
def normal_equation(x, y):
    x_transpose = x.transpose()
    theta = np.linalg.pinv(x_transpose.dot(x)).dot(x_transpose).dot(y)
    y_hat = np.dot(x, theta)
    return theta, y_hat

def RidgeGradientDescent(x, y, alpha, iters, L):
    x=np.matrix(x)
    y=np.matrix(y).transpose()
    # print(y)
    m, n = np.shape(x)
    beta = np.matrix(np.ones(n)).transpose()
    XT = x.transpose()
    for i in range(0, iters):
        y_hat = np.dot(x, beta)
        residuals = y_hat - y
        MSE = (residuals.transpose()*residuals)/len(x)
        # print("iteration:", i, "MSE:", MSE)
        ols_gradient = np.dot(XT, residuals) / m
        beta = beta - alpha * (ols_gradient + (L/m)*beta)
    return beta