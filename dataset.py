"""
2- standardize
"""
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.colors
from sklearn import preprocessing
from regression import gradient_descent
from regression import normal_equation

def make_dataset(row, col, func):
    mat = np.zeros((row, col))

    for index, row in enumerate(mat):
        mat[index][0] = index + 1
        mat[index][1] = math.sin(index + 1)
    return mat


def get_plot(x, y, y_hat = [], x_label = '', y_label = '', title = 'plot', color = ''):
    plt.plot(x, y)
    if y_hat != []:
        plt.plot(x, y_hat)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.savefig(title)
    # plt.show()

# construct a matrix with m rows and n columns
# n can be number of featurs or polynomial degree
def make_polynomial_x(m, degree):
    matrix = np.zeros((m, degree + 1))
    m,n = matrix.shape

    for i in range(0, m):
        for j in range(0, n):
            matrix[i][j] = np.power(x[i], j)

    return matrix

# Feature Scaling or Standardization
def standardize(matrix):
    # m,n = matrix.shape
    #
    # for i in range(0, m):
    #     total = np.sum(matrix[i])
    #     matrix[i] = [x / total for x in matrix[i]]
    mm_scaler = preprocessing.MinMaxScaler()
    matrix = mm_scaler.fit_transform(matrix)

    return matrix

if __name__ == '__main__':
    row = 1000
    col = 2
    func = 'sin(X)'
    dataset = make_dataset(row, col, func)
    x = dataset[:, 0] # inputs
    y = dataset[:, 1] # outputs

    #------------ q1 -----------#
    get_plot(x, y, [], 'x', 'y = ' + func, 'q1 generated dataset')
    plt.clf()

    #------------ q2 -----------#
    print("question 2 :")

    alpha = 0.1
    degree = 3
    iterations = 3000

    y = np.array(y)
    m = len(y) # number of samples

    poly_x = make_polynomial_x(m, degree)
    standard_poly_x = standardize(poly_x.copy())

    theta, y_hat, weighs, MSE =  gradient_descent(standard_poly_x, y,  alpha, 0.5, degree, iterations)

    print("final theta", theta)
    # print("y_hat", y_hat)
    print("MSE:", MSE)
    # print("weighs:", weighs)

    get_plot(x, y, y_hat, 'x', 'y = ' + func, 'q2')
    plt.clf()

    # ------------ q3 -----------#
    print("question 3 :")
    # ----- weighss with regularization -----#

    iters = np.arange(iterations + 1)
    iters = iters[1:]
    lbl = []
    for d in range(0, degree + 1):
        get_plot(iters, weighs[:,d], [], 'iteration', 'theta', 'q3 weighs with regularization')
        lbl.append('θ' + str(d))
    plt.legend(lbl)
    plt.savefig('q3 weighs with regularization')
    # plt.show()
    plt.clf()

    #----- weighss without regularization -----#
    theta, y_hat, weighs, MSE = gradient_descent(standard_poly_x, y, alpha, 0, degree, iterations)
    lbl = []
    for d in range(0, degree + 1):
        get_plot(iters, weighs[:,d], [], 'iteration', 'theta', 'q3 weighs without regularization')
        lbl.append('θ' + str(d))
    plt.legend(lbl)
    plt.savefig('q3 weighs without regularization')
    # plt.show()
    plt.clf()

    #------------ q4 -----------#
    print("question 4 :")

    degree = 3
    iterations = 10000
    alpha_1 = 0.1
    alpha_2 = 0.0005
    alpha_3 = 0.00005

    poly_x = make_polynomial_x(m, degree)
    standard_poly_x = standardize(poly_x.copy())

    theta__1, y_hat_1, weighs_1, MSE_1 = gradient_descent(standard_poly_x, y, alpha_1, 0, degree, iterations)
    theta__2, y_hat_2, weighs_2, MSE_2 = gradient_descent(standard_poly_x, y, alpha_2, 0, degree, iterations)
    theta__3, y_hat_3, weighs_3, MSE_3 = gradient_descent(standard_poly_x, y, alpha_3, 0, degree, iterations)

    print("alpha:", alpha_1, "MSE:", MSE_1)
    print("alpha:", alpha_2, "MSE:", MSE_2)
    print("alpha:", alpha_3, "MSE:", MSE_3)
    # print(theta__1, theta__2, theta__3)

    get_plot(x, y, [], 'x', 'y', 'q4')
    plt.plot(x, y_hat_1, color='red', label="alpha1 = " + str(alpha_1))
    plt.plot(x, y_hat_2, color='green', label="alpha2 = " + str(alpha_2))
    plt.plot(x, y_hat_3, color='orange', label="alpha3 = " + str(alpha_3))
    plt.legend()
    plt.savefig('q4')
    plt.clf()

    # ------------ q5 -----------#
    print("question 5 :")

    degree = 2
    iterations_1 = 100
    iterations_2 = 1000
    iterations_3 = 10000
    alpha = 0.05

    poly_x = make_polynomial_x(m, degree)
    standard_poly_x = standardize(poly_x.copy())

    theta__1, y_hat_1, weighs_1, MSE_1 = gradient_descent(standard_poly_x, y, alpha, 0, degree, iterations_1)
    theta__2, y_hat_2, weighs_2, MSE_2 = gradient_descent(standard_poly_x, y, alpha, 0, degree, iterations_2)
    theta__3, y_hat_3, weighs_3, MSE_3 = gradient_descent(standard_poly_x, y, alpha, 0, degree, iterations_3)

    print("iteraions count:", iterations_1, "MSE:", MSE_1)
    print("iteraions count:", iterations_2, "MSE:", MSE_2)
    print("iteraions count:", iterations_3, "MSE:", MSE_3)
    # print(theta__1, theta__2, theta__3)

    get_plot(x, y, [], 'x', 'y', 'q5')
    plt.plot(x, y_hat_1, color='red', label="iterations_1 = " + str(iterations_1))
    plt.plot(x, y_hat_2, color='green', label="iterations_2 = " + str(iterations_2))
    plt.plot(x, y_hat_3, color='orange', label="iterations_3 = " + str(iterations_3))
    plt.legend()
    plt.savefig('q5')
    plt.clf()

    # ------------ q6 -----------#
    print("question 6 :")

    deg_1 = 2
    deg_2 = 5
    deg_3 = 7
    alpha = 0.05
    iterations = 1000

    poly_x = make_polynomial_x(m, deg_1)
    standard_poly_x = standardize(poly_x.copy())
    theta__1, y_hat_1, weighs_1, MSE_1 = gradient_descent(standard_poly_x, y, alpha, 0, deg_1, iterations)

    poly_x = make_polynomial_x(m, deg_2)
    standard_poly_x = standardize(poly_x.copy())
    theta__2, y_hat_2, weighs_2, MSE_2 = gradient_descent(standard_poly_x, y, alpha, 0, deg_2, iterations)

    poly_x = make_polynomial_x(m, deg_3)
    standard_poly_x = standardize(poly_x.copy())
    theta__3, y_hat_3, weighs_3, MSE_3 = gradient_descent(standard_poly_x, y, alpha, 0, deg_3, iterations)

    print("degree:", deg_1, "MSE:", MSE_1)
    print("degree:", deg_2, "MSE:", MSE_2)
    print("degree:", deg_3, "MSE:", MSE_3)
    # print(theta__1, theta__2, theta__3)

    get_plot(x, y, [], 'x', 'y', 'q6')
    plt.plot(x, y_hat_1, color='red', label="degree_1 = " + str(deg_1))
    plt.plot(x, y_hat_2, color='green', label="degree_2 = " + str(deg_2))
    plt.plot(x, y_hat_3, color='orange', label="degree_3 = " + str(deg_3))
    plt.legend()
    plt.savefig('q6')
    plt.clf()

    # ------------ q8 -----------#
    print("question 8:")

    degree = 2
    alpha = 0.05
    iterations = 1000
    theta_history = []
    lambda_val = [0,0.2,0.4,0.6,0.8]
    poly_x = make_polynomial_x(m, degree)
    standard_poly_x = standardize(poly_x.copy())

    theta_1, y_hat_1, weighs_1, MSE_1 = gradient_descent(standard_poly_x, y, alpha, 0, degree, iterations)
    theta_2, y_hat_2, weighs_2, MSE_2 = gradient_descent(standard_poly_x, y, alpha, 0.2, degree, iterations)
    theta_3, y_hat_3, weighs_3, MSE_3 = gradient_descent(standard_poly_x, y, alpha, 0.4, degree, iterations)
    theta_4, y_hat_4, weighs_4, MSE_4 = gradient_descent(standard_poly_x, y, alpha, 0.6, degree, iterations)
    theta_5, y_hat_5, weighs_5, MSE_5 = gradient_descent(standard_poly_x, y, alpha, 0.8, degree, iterations)

    theta_history = [theta_1]
    theta_history = np.append(theta_history, [theta_2], axis=0)
    theta_history = np.append(theta_history, [theta_3], axis=0)
    theta_history = np.append(theta_history, [theta_4], axis=0)
    theta_history = np.append(theta_history, [theta_5], axis=0)

    lbl = []
    for d in range(0, degree + 1):
        get_plot(lambda_val, theta_history[:, d], [], 'lambda', 'theta', 'q8 weighs according to lambda values')
        lbl.append('θ' + str(d))
    plt.legend(lbl)
    plt.savefig('q8 weighs according to lambda values')
    # plt.show()
    plt.clf()

    # ------------ q9 -----------#
    print("question 9:")

    deg_1 = 2
    deg_2 = 4
    deg_3 = 7

    poly_x = make_polynomial_x(m, deg_1)
    standard_poly_x = standardize(poly_x.copy())
    theta_1, y_hat_1 = normal_equation(standard_poly_x, y)

    poly_x = make_polynomial_x(m, deg_2)
    standard_poly_x = standardize(poly_x.copy())
    theta_2, y_hat_2 = normal_equation(standard_poly_x, y)

    poly_x = make_polynomial_x(m, deg_3)
    standard_poly_x = standardize(poly_x.copy())
    theta_3, y_hat_3 = normal_equation(standard_poly_x, y)

    get_plot(x, y, [], 'x', 'y', 'q9 normal equation')
    get_plot(x, y_hat, [], 'x', 'y', 'q9 normal equation')

    lbl = []
    get_plot(x, y, [], 'x', 'y', 'q9 normal equation')
    get_plot(x, y_hat_1, [], 'x', 'y', 'q9 normal equation')
    lbl.append('degree1 = ' + str(deg_1))
    get_plot(x, y_hat_2, [], 'x', 'y', 'q9 normal equation')
    lbl.append('degree2 = ' + str(deg_2))
    get_plot(x, y_hat_3, [], 'x', 'y', 'q9 normal equation')
    lbl.append('degree3 = ' + str(deg_3))
    plt.legend(lbl)
    plt.savefig('q9 normal equation')
    plt.clf()
